import itertools
import numpy as np
import lastfm
import argparse
import pickle
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as sc
class Hier(object):

    def __init__ (self, n_clusters = 2, names = None):
        self.n_clusters = n_clusters
        self.labels_ = []
        self.data_ = []
        self._cl_num = 0
        self.labels_len_ = 0
        self._np_labels = np.array([])
        self.all_labels_ = []
        self.distances_ = []
        self._labs = []
        self._link = []
        self.names = names
        
    def get_params (self):
        return self.n_clusters

    def set_params (self, n_clusters = 2):
        self.n_clusters = n_clusters

    def fit (self, data):
        self.data_ = data
        n = len(self.data_)
        self.labels_ = range(n)
        self._np_labels = np.array(self.labels_)
        self.all_labels_.append(self.labels_)
        self._cl_num = n
        self.labels_len_ = n
        while (self._cl_num != 1):
            print "number of clusters" + str(self._cl_num)
            (i, j) = self.closest_clusters()
            self.merge_clusters(i, j)
            self._cl_num -= 1
        self.labels_ = self.all_labels_[ - self.n_clusters]
        return self

    def closest_clusters (self):
        closest = ()
        min_distance = -1
        for (i, j) in itertools.combinations(range (self._cl_num), 2):
            distance = self.max_distance(i, j)
            if min_distance != -1:
                if distance < min_distance:
                    min_distance = distance
                    closest = (i, j)
            else:
                min_distance = distance
                closest = (i, j)
        self.distances_.append(min_distance)
        return closest

    def max_distance (self, i , j):
        fst = np.where(self._np_labels == i)[0]
        snd = np.where(self._np_labels == j)[0]
        max_d = -1
        for (f, s) in itertools.product(fst, snd):          
            dist = self.jacqard_distance(f , s)
            if dist > max_d:
                max_d = dist
        return max_d

    def jacqard_distance(self, fst , snd):   # for our task
        distance = 1
        for each in zip(self.data_[fst], self.data_[snd]):
            distance *= self.jacqard_metric(each)
        return distance

    def jacqard_metric(self, data):
        first = data[0]
        second = data[1]
        same = len(first & second)
        d = len(first) + len(second) - same
        distance = 1 if d == 0 else 1 - 1.0 * same / d
        return distance
                
    def merge_clusters (self, i, j):
        if i > j:                   # i = min
            i, j = j, i
        self.labels_ = [k if k < j else i if k == j else k - 1 for k in self.labels_]
        self._np_labels = np.array(self.labels_)
        self.all_labels_.append(self.labels_)

    def euclid_distance(self,  fst, snd):    # for numbers
        first = self.data_[fst]
        second = self.data_[snd]
        summ = 0
        for f, s in zip(first, second):
            summ += (f - s) ** 2
        return summ ** 1.0/2

    def make_dendrogram(self):
        self._labs = self.all_labels_[0]
        it = 0
        for (i, j, d) in zip(self.all_labels_[0 : -1], self.all_labels_[1 :], self.distances_):
            min_el = -1  #find element and its index which was changed
            min_ind = 0
            for (ind, el) in enumerate(i):
                if (el != j[ind]) and ((min_el >= 0) and (min_el > el) or (min_el < 0)):
                    min_el = el
                    min_ind = ind

            prev = min_el  # old num of cluster before merging
            succ = j[min_ind]  # new num of cluster after merging
            old = self._labs[min_ind]       # first old num of cluster for merging in special list for linkage matrix
            new_cl = self.labels_len_ + it  # number of new cluster for linkage matrix
            m = -1   # second old num of cluster for merging in special list for linkage matrix
            c = 0
            while m == -1:
                if (i[c] == succ):
                    m = c
                c += 1

            self._link.append([old, self._labs[m], self.distances_[it], 0])
            self._labs = [new_cl if ((i[ind] == prev) or (j[ind] == succ)) else el for (ind, el) in enumerate(self._labs)]    
            
            it += 1
        if (self.names != None) and (len(self.names) == (self.labels_len_)):
            sc.dendrogram(self._link, color_threshold = self.distances_[-self.n_clusters + 1], labels = self.names)
        else:
            sc.dendrogram(self._link, color_threshold = self.distances_[-self.n_clusters + 1])
        plt.show()



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", action="store", help = "file with data", default = "poprockrap.txt")
    #parser.add_argument("-n", "--number", action="store", help = "number of clusters", default = 3)
    args = parser.parse_args()
    p = pickle.Unpickler(open(args.file, 'r'))
    (n_cl, names, data) = p.load()
    a = Hier(n_cl, [each[0] for each in names])
    a.fit(data)
    print a.labels_
    a.make_dendrogram()
    

if __name__ == "__main__":
    main()

