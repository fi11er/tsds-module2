Author Valerie Mozharova.

Hierarchical algorithm.

Program lastfm.py extracts data from lastfm.com (festivals, fans, artist summary).

File poprockrap.txt contains extracted data (artists with tags: pop, rock, rap).

Program hierarchical.py clusters data with hierarchical algorithm and makes dendrogram.
