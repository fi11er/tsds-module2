import random
import numpy as np


class Kmeans(object):

    def __init__(self, n_clusters=8, n_init=25, max_iter=300):
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.n_init = n_init

    def fit(self, X):
        random.seed()
        s = X.shape
        center = np.zeros(self.n_clusters * s[1])
        center.shape = (self.n_clusters, s[1])
        r = np.zeros(s[0] * self.n_clusters)
        r.shape = (s[0], self.n_clusters)
        for i in range(0, self.n_clusters):
            center[i] = X[random.randint(0, s[0] - 1)]
        for j in (0, self.max_iter):
            for k in range(0, s[0]):
                temp = ((X[k] - center)**2).sum(1)
                arg = temp.argmin()
                r[k] = temp != temp
                r[k][arg] = 1
            sum = r.sum(0)
            sum.shape = (sum.size, 1)
            center = np.dot(np.transpose(r), X) / sum
        best_l = ((np.dot(r, center) - X)**2).sum()
        best_center = center
        best_r = r


        for i in range(1, self.n_init - 1):
            for i in range(0, self.n_clusters):
                center[i] = X[random.randint(0, s[0] - 1)]

            for j in (0, self.max_iter):
                for k in range(0, s[0]):
                    temp = ((X[k] - center)**2).sum(1)
                    arg = temp.argmin()
                    r[k] = temp != temp
                    r[k][arg] = 1
                sum = r.sum(0)
                sum.shape = (sum.size, 1)
                center = np.dot(np.transpose(r), X) / sum

            l = ((np.dot(r, center) - X)**2).sum()
            if l < best_l:
                best_center = center
                best_r = r
                best_l = l

        self.cluster_centers_ = best_center
        self.labels_ = np.transpose([elem.argmax() for elem in best_r])

    def fit_predict(self, X):
        self.fit(X)
        return self.labels_