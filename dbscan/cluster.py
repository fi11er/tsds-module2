import lastfm
from dbscan import Dbscan
import pickle
from sklearn.feature_extraction.text import CountVectorizer
import argparse


def make_features(key, tags):
	client = lastfm.ApiClient(key)
	features = []
	names = []
	analize = CountVectorizer().build_analyzer()
	for tag in tags:
		for artist_name in client.top_artist_names(tag):
			try:
				print "getting by tag " + tag
				artist = client.load_artist(artist_name)
				if len(artist.events) > 0 or len(artist.fans) > 0:
					names.append([artist.name, tag])
					features.append([artist.events, artist.fans, set(analize(artist.summary))])
					print "loaded"
			except:
				print "failed to load artist"
	return names, features

def save_features(names, features, file):
	f = open(file, 'w+')
	p = pickle.Pickler(f)
	p.dump((names, features))
	f.close()
	
def load_features(file):
	f = open(file, 'r+')
	u = pickle.Unpickler(f)
	return u.load()
	
	
def parse_args():
	parser = argparse.ArgumentParser(description = 'Clustering lastfm data, using dbscan algorithm')
	parser.add_argument("-eps", type=float, default=0.78, help="eps param for searching neighbors")
	parser.add_argument("-minsamples", type=int, default=40, help="min neighbors param for making sample as core")
	parser.add_argument("-metric", type=str, default="lastfm", help="metric for dbscan distance")
	subparsers = parser.add_subparsers(help="commands", dest="command")
	fromfile_parser = subparsers.add_parser("fromfile", help="get data from file")
	fromfile_parser.add_argument('-file', metavar='F', type=str, default="data.txt", help='name of file with data')
	fromsite_parser = subparsers.add_parser("fromsite", help="get data from site")
	fromsite_parser.add_argument('-token', metavar='T', type=str, default='24c22ffe980cd4b171662195556da170', help='token for lastfm api')
	fromsite_parser.add_argument('tags', type=str, nargs='+', help='tags to get from lastfm')
	fromsite_parser.add_argument('-outfile', metavar='OUT', type=str, help='name of file to save data')
	return parser.parse_args()
	
def main():
	args = parse_args()
	if args.command == "fromfile":
		print "loading data from file..."
		names, features = load_features(args.file)
	elif args.command == "fromsite":
		print "downloading data from lastfm..."
		names, features = make_features(args.token, args.tags)
		if args.outfile is not None:
			print "saving data to file..."
			save_features(names, features, args.outfile)

	print "Number of samples: ", len(features)
	db = Dbscan(eps = args.eps, min_samples = args.minsamples, metric=args.metric).fit(features)
	print db.labels_
	
if __name__ == "__main__":
	main()
	



