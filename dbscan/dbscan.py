import math
import itertools

class Dbscan(object):
	
	def __init__(self, eps = 0.5, min_samples = 5, metric='euclidean'):
		self.eps = eps
		self.min_samples = min_samples
		if hasattr(self, "_" + metric + "_metric"):
			self._metric = getattr(self, "_" + metric + "_metric")
		else:
			print "unknown metric, using euclidean instead"
			self._metric = self._euclidean_metric
	
	def fit(self, X):
		self._samples = [sample for sample in enumerate(X)]
		self._visited = set()
		self._clusters = -1
		self.core_sample_indices_ = []
		self.labels_ = [-1] * len(X)
		
		for i, x in self._samples:
			if not i in self._visited:
				self._visited.add(i)
				nbr = self._neighbors(x)
				print "sample #", i
				if len(nbr) < self.min_samples:
					self.labels_[i] = -1
				else:
					self.core_sample_indices_.append(i)
					self._clusters += 1
					c = self._clusters
					self._expand_cluster(nbr, c)	
		print "done"
		return self
					
					
	def _expand_cluster(self, nbr, c):
		for i, y in nbr:
			if not i in self._visited:
				self._visited.add(i)
				nbr2 = self._neighbors(y)
				if len(nbr2) >= self.min_samples:
					nbr.extend(nbr2)
					self.core_sample_indices_.append(i)
			if not self.labels_[i] >= 0:
				self.labels_[i] = c
			
	def _neighbors(self, x):
		return [y for y in self._samples if self._metric(x, y[1]) < self.eps]
		
	def _euclidean_metric(self, x, y):
		sum = 0;
		for i in range(len(x)):
			sum += (x[i] - y[i]) ** 2
		return math.sqrt(sum)	
					
	def _lastfm_metric(self, x, y):
		assert len(x) == len(y), "no the same length!"
		m = 1
		for i in range(len(x)):
			m *= self._jacqard_metric(x[i], y[i])
		return m
		
	def _jacqard_metric(self, x, y):
		xl = len(x)
		yl = len(y)
		samel = len(x&y)
		if xl == 0 or yl == 0:
			m = 1.0
		else:
			m = 1 - 1.0 * samel / (xl + yl - samel)
		return m
		
	def print_metrics(self, X): #used only for searching good coefs
		f = open("metrics.csv", 'w+')
		for x in X:
			f.write('\t'.join([str(self._metric(x, y)) for y in X]) + '\n')
		f.close()
		
	def print_neighbors(self, X, n = 3, size = 100): #used only for searching good coefs
		f = open("neighbors.csv", 'w+')
		for k, x in enumerate(X):
			f.write(str(k))
			for i in range(n):
				Y = X[i*size:(i+1)*size]
				num = len([y for y in Y if self._metric3(x, y) < self.eps])
				f.write('\t' + str(num))
			f.write('\n')
		f.close()